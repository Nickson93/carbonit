package carbonIT.fileUtils;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.List;

public class JeuxWriter {
	
	public static void write(List<String> lignes, String chemin){
		File fichier =  new File(chemin);
		
		try (Writer writer = new FileWriter(fichier)) {
			lignes.forEach(ligne -> {
				try {
					writer.write(ligne.toString());
					writer.write("\n");
				} catch (IOException e) {
					System.out.println("Erreur " + e.getMessage()) ;
					e.printStackTrace();
				}
			});
		} catch (IOException e) {
			System.out.println("Erreur " + e.getMessage()) ;
			e.printStackTrace();
		}
		
		
	}
}
