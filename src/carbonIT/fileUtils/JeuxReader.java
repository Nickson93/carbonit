package carbonIT.fileUtils;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import carbonIT.Jeux;
import carbonIT.Enum.ElementEnum;
import carbonIT.element.Aventurier;
import carbonIT.element.Carte;
import carbonIT.element.CaseMontagne;
import carbonIT.element.CaseTresor;
import carbonIT.parser.Parser;

public class JeuxReader {
	private static final Logger LOGGER = Logger.getLogger( JeuxReader.class.getName() );
	/**
	 * Lecture du fichier d�crivant le jeux
	 * @param chemin
	 * @return
	 */
	public static List<String[]> lireFichierJeux(String chemin) throws IOException {
		// On r�cup�re les lignes brutes du fichier
		List<String> listOflines = listOflines(chemin);
		// On format nos ligne pour les rendrent exploitable
		return formatLigne(listOflines);
	}
	
	/**
	 * @param chemin
	 * @return
	 * @throws IOException 
	 */
	public static List<String> listOflines (String chemin) throws IOException{
		Path path = Paths.get(chemin);
		try (Stream<String> lines = Files.lines(path)){
			return lines.collect(Collectors.toList());
		}catch (IOException e){
			LOGGER.severe("e.getMessage() ="+ e.getMessage());
			throw new IOException();
		}
	}
	
	/**
	 * Supression des lignes comment�e 
	 * et split de chaque ligne pour faciliter la r�cup�ration des informations
	 * @param lignes
	 * @return
	 */
	private static List<String[]> formatLigne(List<String> lignes) {
		
		return lignes.stream().filter(ligne -> !ligne.trim().isEmpty())
							  .filter(ligne -> ligne.charAt(0)!='#')
						 	  .map(ligne -> ligne.split(" - "))
						 	  .collect(Collectors.toList());
	}
	
}
