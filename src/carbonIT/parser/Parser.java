package carbonIT.parser;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import carbonIT.Jeux;
import carbonIT.Enum.ElementEnum;
import carbonIT.Enum.OrientationEnum;
import carbonIT.element.Aventurier;
import carbonIT.element.Carte;
import carbonIT.element.CaseMontagne;
import carbonIT.element.CaseTresor;

public class Parser {
	private static final Logger LOGGER = Logger.getLogger( Parser.class.getName() );
	/**
	 * 
	 * @param ligne
	 * @return
	 */
	private static Carte parseLigneToCarte(String[] ligne) throws NumberFormatException{
		return new Carte(Integer.valueOf(ligne[2]), Integer.valueOf(ligne[1]));
	}
	
	/**
	 * 
	 * @param ligne
	 * @return
	 */
	private static CaseMontagne parseLigneToCaseMontagne(String[] ligne) throws NumberFormatException{
		return new CaseMontagne(Integer.valueOf(ligne[2]), Integer.valueOf(ligne[1]));
	}
	
	/**
	 * 
	 * @param ligne
	 * @return
	 */
	private static CaseTresor parseLigneToCaseTresor(String[] ligne) {
		return new CaseTresor(Integer.valueOf(ligne[2]), Integer.valueOf(ligne[1]), Integer.valueOf(ligne[3]));
	}
	
	/**
	 * 
	 * @param ligne
	 * @return
	 */
	private static Aventurier parseLigneToAventurier(String[] ligne) throws NumberFormatException{
		return new Aventurier(ligne[1],
							  Integer.valueOf(ligne[3]),
							  Integer.valueOf(ligne[2]),
							  OrientationEnum.valueOf(ligne[4]),
							  ligne[5]);
	}
	
	public static Jeux parseLigneToJeux(List<String[]> ligneToConvert)  throws NumberFormatException{
		Jeux jeux = new Jeux();
		Boolean parsingOK= true;
		//Chaque ligne est convertie en �l�ment du jeux puis renseign� dans notre objet Jeux
		for (Integer numLigne = 0;  numLigne < ligneToConvert.size(); numLigne++) {
			try{
				convertLigne(ligneToConvert.get(numLigne), jeux, numLigne);
			} catch (NumberFormatException numberFormatException){
				LOGGER.severe("La ligne " + numLigne + " pr�sente une erreur de format");
				parsingOK = false;
			}
		}
		if (parsingOK) { 
			return jeux;
		}else{
			throw new NumberFormatException();
		}
	}
	
	/**
	 * 
	 * @param ligne
	 * @param jeux
	 * @param numLigne 
	 */
	private static void convertLigne(String[] ligne, Jeux jeux, Integer numLigne) throws NumberFormatException{
		switch(ElementEnum.valueOf(ligne[0])) {
		case C:
		    Carte carte = Parser.parseLigneToCarte(ligne);
		    jeux.setMadreDedios(carte);
		    break;
		case M:
		    CaseMontagne montagne = Parser.parseLigneToCaseMontagne(ligne);
		    jeux.addMontagnes(montagne);
		    break;
		case T:
			CaseTresor tresor = Parser.parseLigneToCaseTresor(ligne);
		    jeux.addTresor(tresor);
		    break;
		case A:
			Aventurier aventurier = Parser.parseLigneToAventurier(ligne);
		    jeux.addAventurier(aventurier);
			break;
		  default:
		    LOGGER.warning("La ligne " + numLigne + " n'est pas reconnu et ne sera pas trait�!");
		}
	}
	
	
	/**
	 * 
	 * @param carte
	 * @return
	 */
	private static String parseCarteToLigne(Carte carte) {		
		return Arrays.asList(ElementEnum.C.getElement(),
							 carte.getSizeY().toString(),
							 carte.getSizeX().toString())
					 .stream()
					 .collect(Collectors.joining(" - "));
	}
	
	/**
	 * 
	 * @param montagne
	 * @return
	 */
	private static String parseCaseMontagneToLigne(CaseMontagne montagne) {
		return Arrays.asList(ElementEnum.M.getElement(),
				 			 montagne.getY().toString(),
							 montagne.getX().toString()).stream()
				 	 .collect(Collectors.joining(" - "));
	}
	
	/**
	 * 
	 * @param tresor
	 * @return
	 */
	private static String parseCaseTresorToLigne(CaseTresor tresor) {
		return Arrays.asList(ElementEnum.T.getElement(),
				 			 tresor.getY().toString(),
							 tresor.getX().toString(),
							 tresor.getNombreDeTresor().toString())
					 .stream()
					 .collect(Collectors.joining(" - "));
	}
	
	/**
	 * 
	 * @param ligne
	 * @return
	 */
	private static String parseCaseAventurierToLigne(Aventurier aventurier) {
		return Arrays.asList(ElementEnum.A.getElement(),
							 aventurier.getNom(),
							 aventurier.getPositionY().toString(),
							 aventurier.getPositionX().toString(),
							 aventurier.getOrientation().getOrientation(),
							 aventurier.getNombreDeTresor().toString())
					 .stream()
					 .collect(Collectors.joining(" - "));
	}
	
	/**
	 * Lignes r�sument le jeux
	 * @param jeux
	 * @return
	 */
	public static List<String> parseJeuxToLignes(Jeux jeux) {
		Stream<String> ligneCarte = Stream.of(parseCarteToLigne(jeux.getMadreDedios()));
		Stream<String> lignesMontagnes = jeux.getMontagnes().stream()
														  .map(Parser::parseCaseMontagneToLigne);
		Stream<String> lignesTresors = jeux.getTresors().stream()
				  										.map(Parser::parseCaseTresorToLigne);
		Stream<String> lignesAventutiers = jeux.getAventuriers().stream()
				  												.map(Parser::parseCaseAventurierToLigne);
		return Stream.of(ligneCarte, lignesMontagnes, lignesTresors, lignesAventutiers)
									.flatMap(i -> i)
									.collect(Collectors.toList());
	}
}
