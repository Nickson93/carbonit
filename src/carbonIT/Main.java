package carbonIT;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import carbonIT.fileUtils.JeuxReader;
import carbonIT.fileUtils.JeuxWriter;
import carbonIT.parser.Parser;

public class Main {

	public static void main(String[] args) {
		String directory = "C:/Users/nsivapat/eclipse-workspace/carbonIT/src/files/";
		List<String[]> lignesJeux;
		try {
			lignesJeux = JeuxReader.lireFichierJeux(directory + "MadreDeDios");
			Jeux jeux = Parser.parseLigneToJeux(lignesJeux);
			jeux.initialiserJeux();
			
			jeux.lancerPartie();
			System.out.println(jeux.afficherResultatFinal());
			
			List<String> jeuxFiniEnLignes = Parser.parseJeuxToLignes(jeux);
			JeuxWriter.write(jeuxFiniEnLignes, directory + "Resulats");
		} catch (IOException | NumberFormatException e) {
			e.printStackTrace();
		} 
		
	}
}

