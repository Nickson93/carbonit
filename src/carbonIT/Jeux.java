package carbonIT;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import carbonIT.element.Aventurier;
import carbonIT.element.Carte;
import carbonIT.element.CaseMontagne;
import carbonIT.element.CaseTresor;

public class Jeux {
	private Carte madreDedios;
	private List<CaseMontagne> montagnes = new ArrayList<>();
	private List<CaseTresor> tresors = new ArrayList<>();
	private List<Aventurier> aventuriers = new ArrayList<>();
	
	/**
	 * 
	 * @return
	 */
	public Carte getMadreDedios() {
		return madreDedios;
	}
	
	/**
	 * 
	 * @param madreDedios
	 */
	public void setMadreDedios(Carte madreDedios) {
		this.madreDedios = madreDedios;
	}
	
	/**
	 * 
	 * @return
	 */
	public List<CaseMontagne> getMontagnes() {
		return montagnes;
	}
	
	/**
	 * 
	 * @return
	 */
	public List<CaseTresor> getTresors() {
		return tresors;
	}
	
	/**
	 * 
	 * @return
	 */
	public List<Aventurier> getAventuriers() {
		return aventuriers;
	}
	
	/**
	 * 
	 * @param montagne
	 */
	public void addMontagnes(CaseMontagne montagne) {
		this.getMontagnes().add(montagne);
	}

	/**
	 * 
	 * @param tresor
	 */
	public void addTresor(CaseTresor tresor) {
		this.getTresors().add(tresor);
	}
	
	/**
	 * 
	 * @param aventurier
	 */
	public void addAventurier(Aventurier aventurier) {
		this.getAventuriers().add(aventurier);
	}
	
	/**
	 * Initialisation de la carte apr�s avoir remplit notre objet
	 */
	public void initialiserJeux() {
		// Positionnement des Montagnes et Tr�sor sur la carte
		Stream.of(this.getMontagnes(), this.getTresors())
			  .flatMap(list -> list.stream())
			  .forEach(caseElement -> this.madreDedios.positionnerCase(caseElement));
		
		// Positionnement des aventurier sur la carte
		aventuriers.stream().forEach(aventurier -> this.madreDedios.positionnerAventurier(aventurier));
	}
	
	/**
	 * Lancement de la partie
	 */
	public void lancerPartie() {
		// Le maximum de tour a faire
		Integer nombreDeTour = aventuriers.stream().map(Aventurier::getSequenceDeMouvement)
												   .mapToInt(String::length)
												   .max()
												   .orElse(0);
		
		// On execute le tour i pour chacun des aventuriers
		for (Integer i = 0; i < nombreDeTour; i++) {
			System.out.println("Tour " + (i+1) + " \n" + this.madreDedios);
			executerTour(i);
		}
	}

	/**
	 * On d�place chaque aventurier 
	 * @param tour
	 */
	private void executerTour(Integer tour) {
		String sequenceDeMouvement;
		for (Aventurier aventurier : aventuriers){
			sequenceDeMouvement = aventurier.getSequenceDeMouvement();
			if(sequenceDeMouvement.length() >= tour + 1) {
			    String action = sequenceDeMouvement.substring(tour, tour + 1);
			    System.out.println(aventurier.getNom() + " va tenter l\'action suivante " + action);
			    madreDedios.deplacerAventurier(aventurier, action);
			}
		}
	}
	
	public String afficherResultatFinal() {
		StringBuilder builder = new StringBuilder("R�sultat final\n");
		builder.append(this.madreDedios).append("\nNombre de tr�sor trouv�(s):\n");
		aventuriers.stream().forEach(aventurier -> builder.append(aventurier.getNom())
														  .append(" � trouv� ")
														  .append(aventurier.getNombreDeTresor())
														  .append(" tr�sor(s)\n"));
		builder.append("Les cases tr�sors sont:\n");
		tresors.stream().forEach(tresor -> builder.append("Il reste ")
												  .append(tresor.getNombreDeTresor())
												  .append(" tr�sor(s) aux coordon�es X:")
												  .append(tresor.getX())
												  .append(" Y:")
												  .append(tresor.getY())
												  .append("\n"));
		return builder.toString();
	}
}
