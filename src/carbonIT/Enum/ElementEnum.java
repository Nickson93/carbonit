package carbonIT.Enum;

public enum ElementEnum {
	C("C"),
	M("M"),
	T("T"),
	A("A");
	
	String element;
	
	private ElementEnum(String element) {
		this.element = element;
	}
	
	public String getElement() {  
        return  this.element ;  
   }  
}
