package carbonIT.Enum;

import carbonIT.element.Aventurier;

public enum OrientationEnum {
	N("N") {
		@Override
		public void gauche(Aventurier a) {
			a.setOrientation(OrientationEnum.O);
		}

		@Override
		public void droite(Aventurier a) {
			a.setOrientation(OrientationEnum.E);
		}

		@Override
		public void avance(Aventurier a) {
			a.setPositionX(a.getPositionX() - 1);
		}
	},
	S("S") {
		@Override
		public void gauche(Aventurier a) {
			a.setOrientation(OrientationEnum.E);
		}

		@Override
		public void droite(Aventurier a) {
			a.setOrientation(OrientationEnum.O);
		}

		@Override
		public void avance(Aventurier a) {
			a.setPositionX(a.getPositionX() + 1);
		}
	},
	E("E") {
		@Override
		public void gauche(Aventurier a) {
			a.setOrientation(OrientationEnum.N);
		}

		@Override
		public void droite(Aventurier a) {
			a.setOrientation(OrientationEnum.S);
		}

		@Override
		public void avance(Aventurier a) {
			a.setPositionY(a.getPositionY() + 1);
		}
	},
	O("O") {
		@Override
		public void gauche(Aventurier a) {
			a.setOrientation(OrientationEnum.S);
		}

		@Override
		public void droite(Aventurier a) {
			a.setOrientation(OrientationEnum.N);
		}

		@Override
		public void avance(Aventurier a) {
			a.setPositionY(a.getPositionY() - 1);
		}
	};
	
	public abstract void gauche(Aventurier a);
	public abstract void droite(Aventurier a);
	public abstract void avance(Aventurier a);

	String orientation;
	
	private OrientationEnum(String orientation) {
		this.orientation = orientation;
	}
	
	public String getOrientation() {  
        return  this.orientation ;  
   }  
}
