package carbonIT.Enum;

import carbonIT.element.Aventurier;

public enum ActionEnum {
	A("A"){
		@Override
		public void perform(Aventurier a) {
			a.getOrientation().avance(a);
		}
	},
	D("D") {
		@Override
		public void perform(Aventurier a) {
			a.getOrientation().droite(a);
		}
	},
	G("G") {
		@Override
		public void perform(Aventurier a) {
			a.getOrientation().gauche(a);
		}
	};
	String action;
	
	private ActionEnum(String action) {
		this.action = action;
	}
	
	public String getAction() {  
        return  this.action ;  
   }  
	public abstract void perform(Aventurier a);
}
