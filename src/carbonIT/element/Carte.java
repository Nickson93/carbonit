package carbonIT.element;

import java.util.logging.Logger;

import carbonIT.Enum.ActionEnum;
import carbonIT.exception.InitialisationException;
import carbonIT.parser.Parser;

public class Carte {
	private static final Logger LOGGER = Logger.getLogger( Carte.class.getName() );
	private Case[][] carte;
	private Integer sizeX;
	private Integer sizeY;
	
	public Carte(Integer sizeX, Integer sizeY) {
		this.sizeX = sizeX;
		this.sizeY = sizeY;
		carte = new Case[sizeX][sizeY];
		
		for(Integer i=0; i<sizeX; i++) {
			for(Integer j=0; j<sizeY; j++) {
				carte[i][j] = new Case(i,j); 
			}
		}
	}
	
	/**
	 * 
	 * @return
	 */
	public Integer getSizeX() {
		return sizeX;
	}

	/**
	 * 
	 * @param sizeX
	 */
	public void setSizeX(Integer sizeX) {
		this.sizeX = sizeX;
	}

	/**
	 * 
	 * @return
	 */
	public Integer getSizeY() {
		return sizeY;
	}

	/**
	 * 
	 * @param sizeY
	 */
	public void setSizeY(Integer sizeY) {
		this.sizeY = sizeY;
	}


	/**
	 * 
	 * @param elementCase
	 */
	public void positionnerCase(Case elementCase) {
		try {
			controleAvantPositionnementDesCase(elementCase);
			this.carte[elementCase.getX()][elementCase.getY()] = elementCase;
		} catch(InitialisationException e) {
			LOGGER.warning(e.getMessage());
		}
	}

	/**
	 * On v�rifie que la case peut etre placer sur la carte 
	 * ou que sa position ne soit pas d�ja occup�
	 * @param elementCase
	 * @return
	 * @throws InitialisationException 
	 */
	private void controleAvantPositionnementDesCase(Case elementCase) throws InitialisationException {
		if (elementCase.getX() > sizeX || elementCase.getY() > sizeY) {
			LOGGER.warning(elementCase + " " +elementCase.getX()+":"+elementCase.getY()
						   + " est en dehors des dimensions de la carte. Il ne sera pas int�gr� au jeux!");
			throw new InitialisationException();
		}
		Case caseToReplace = this.carte[elementCase.getX()][elementCase.getY()];
		if ( caseToReplace instanceof CaseMontagne ||
			 caseToReplace instanceof CaseTresor) {
			LOGGER.warning(elementCase + " " +elementCase.getX()+":"+elementCase.getY()
			   + "Un �l�ment existe d�j� � cette endroit. Il ne sera pas int�gr� au jeux!");
			throw new InitialisationException();
		}
	}
	
	/**
	 * 
	 * @param aventurier
	 */
	public void positionnerAventurier(Aventurier aventurier) {
		
			try {
				Case caseToPlace = controleAvantPositionnementDesAventurier(aventurier);
				this.carte[aventurier.getPositionX()][aventurier.getPositionY()].setAventurier(aventurier);
			} catch (InitialisationException e) {
				e.printStackTrace();
			}
	}
	
	private Case controleAvantPositionnementDesAventurier(Aventurier aventurier) throws InitialisationException {
		Case caseToPlace = this.getCase(aventurier.getPositionX(),aventurier.getPositionY());
		if (caseToPlace instanceof CaseMontagne || caseToPlace.getAventurier() != null) {
			LOGGER.warning("Impossible de placer un aventurier sur une montagne ou sur un autre aventurier."
					+ aventurier.getNom() + " ne sera pas int�gr� au jeux!");
			throw new InitialisationException();
		}
		return caseToPlace;
	}
	/**
	 * 
	 * @param aventurier
	 * @param action
	 */
	public void deplacerAventurier(Aventurier aventurier, String action) {
		Case caseActuelle;
		Case nouvelleCase;
		Aventurier aventurierAvecNouvellePosition;

		// On cr�e un nouvelle aventurier avec sa futur position potentiel
		aventurierAvecNouvellePosition = new Aventurier(aventurier);
		ActionEnum actionEnum = ActionEnum.valueOf(action);
		actionEnum.perform(aventurierAvecNouvellePosition);
		
	    try {
		    nouvelleCase = this.getCase(aventurierAvecNouvellePosition.getPositionX(), aventurierAvecNouvellePosition.getPositionY());
		    // Dans le cas d'une rotation aucune v�rification n'est n�cessaire
		    if (ActionEnum.D.equals(actionEnum) || ActionEnum.G.equals(actionEnum)) { 
		    	aventurier.setAventurier(aventurierAvecNouvellePosition);
		    }
		    /* Dans le cas d'un d�placement il faut v�rifier si la nouvelle case peut l'accueillir
		       diff�rentes actions seront ex�cut� selon le type de case ou notre aventurier atterit*/
		    else if (nouvelleCase.accueillir(aventurierAvecNouvellePosition)) {
		    	caseActuelle = this.getCase(aventurier.getPositionX(), aventurier.getPositionY());
				caseActuelle.setAventurier(null);
				aventurier.setAventurier(aventurierAvecNouvellePosition);
				positionnerAventurier(aventurier);
		    }
	    } catch (ArrayIndexOutOfBoundsException e){
	    	System.out.println("Impossible de se rendre ici " + aventurier.getNom());
	    }
	}
	
	/**
	 * 
	 * @param X
	 * @param Y
	 * @return
	 * @throws ArrayIndexOutOfBoundsException
	 */
	public Case getCase(Integer X, Integer Y) throws ArrayIndexOutOfBoundsException{
		return carte[X][Y];
	}
	
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder("============================ Carte ==============================\n");
		for(Integer i=0; i< carte.length ; i++) {
			for(Integer j=0; j< carte[i].length ; j++) {
				builder.append(carte[i][j].toString()).append(" "); 
			}
			builder.append("\n");
		}
		builder.append("=================================================================\n");
		return builder.toString();
	}
		
}
