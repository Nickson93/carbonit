package carbonIT.element;

public class Case{
	protected Integer x;
	protected Integer y;
	protected Aventurier aventurier;
	
	public Aventurier getAventurier() {
		return aventurier;
	}
	public void setAventurier(Aventurier aventurier) {
		this.aventurier = aventurier;
	}
	protected Case(Integer x, Integer y) {
		super();
		this.setX(x);
		this.setY(y);
	}
	public Integer getX() {
		return x;
	}
	protected void setX(Integer x) {
		this.x = x;
	}
	public Integer getY() {
		return y;
	}
	protected void setY(Integer y) {
		this.y = y;
	}

	@Override
	public String toString() {
		return aventurier == null ? ".\t\t" : "A(" + this.getAventurier().getNom() + ")\t";
	}
	
	public Boolean accueillir(Aventurier newAventurier) {
		if (this.getAventurier() == null) {
			return true;
		} else {
			System.out.println("Un aventurier est d�j� pr�sent ici " + newAventurier.getNom() + " !!!");
			return false;
		}
	}

}
