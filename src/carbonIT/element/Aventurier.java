package carbonIT.element;

import carbonIT.Enum.OrientationEnum;

public class Aventurier{

	private String nom;
	private Integer positionX;
	private Integer positionY;
	private OrientationEnum orientation;
	private Integer nombreDeTresor = 0;
	private String sequenceDeMouvement;

	public Aventurier(String nom, Integer positionX, Integer positionY, OrientationEnum orientation,
			String sequenceDeMouvement) {
		super();
		this.nom = nom;
		this.positionX = positionX;
		this.positionY = positionY;
		this.orientation = orientation;
		this.sequenceDeMouvement = sequenceDeMouvement;
	}
	
	public Aventurier(Aventurier autreAventurier) {
		this(autreAventurier.getNom(),
			 autreAventurier.getPositionX(),
			 autreAventurier.getPositionY(), 
			 autreAventurier.getOrientation(),
			 autreAventurier.getSequenceDeMouvement());
		this.nombreDeTresor = autreAventurier.getNombreDeTresor();
	}
	
	public Integer getNombreDeTresor() {
		return nombreDeTresor;
	}

	public void setNombreDeTresor(Integer nombreDeTresor) {
		this.nombreDeTresor = nombreDeTresor;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public OrientationEnum getOrientation() {
		return orientation;
	}

	public void setOrientation(OrientationEnum orientation) {
		this.orientation = orientation;
	}

	public String getSequenceDeMouvement() {
		return sequenceDeMouvement;
	}

	public void setSequenceDeMouvement(String sequenceDeMouvement) {
		this.sequenceDeMouvement = sequenceDeMouvement;
	}

	public Integer getPositionX() {
		return positionX;
	}

	public void setPositionX(Integer positionX) {
		this.positionX = positionX;
	}

	public Integer getPositionY() {
		return positionY;
	}

	public void setPositionY(Integer positionY) {
		this.positionY = positionY;
	}
	
	public void setAventurier(Aventurier newAventurier) {
		this.positionX = newAventurier.getPositionX();
		this.positionY = newAventurier.getPositionY();
		this.orientation = newAventurier.getOrientation();
		this.nombreDeTresor = newAventurier.getNombreDeTresor();
	}

	@Override
	public String toString() {
		return "A";
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((nom == null) ? 0 : nom.hashCode());
		result = prime * result + ((positionX == null) ? 0 : positionX.hashCode());
		result = prime * result + ((positionY == null) ? 0 : positionY.hashCode());
		result = prime * result + ((sequenceDeMouvement == null) ? 0 : sequenceDeMouvement.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Aventurier other = (Aventurier) obj;
		if (nom == null) {
			if (other.nom != null)
				return false;
		} else if (!nom.equals(other.nom))
			return false;
		if (positionX == null) {
			if (other.positionX != null)
				return false;
		} else if (!positionX.equals(other.positionX))
			return false;
		if (positionY == null) {
			if (other.positionY != null)
				return false;
		} else if (!positionY.equals(other.positionY))
			return false;
		if (sequenceDeMouvement == null) {
			if (other.sequenceDeMouvement != null)
				return false;
		} else if (!sequenceDeMouvement.equals(other.sequenceDeMouvement))
			return false;
		if (nombreDeTresor == null) {
			if (other.nombreDeTresor != null)
				return false;
		} else if (!nombreDeTresor.equals(other.nombreDeTresor))
			return false;
		return true;
	}
}
