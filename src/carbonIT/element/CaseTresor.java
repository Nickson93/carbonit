package carbonIT.element;

public class CaseTresor extends Case {

	private Integer nombreDeTresor;

	public CaseTresor(Integer x, Integer y, Integer nombreDeTresor) {
		super(x, y);
		this.setNombreDeTresor(nombreDeTresor);
	}

	public Integer getNombreDeTresor() {
		return nombreDeTresor;
	}

	private void setNombreDeTresor(Integer nombreDeTresor) {
		this.nombreDeTresor = nombreDeTresor;
	}

	@Override
	public String toString() {
		String toString = "T(" + this.getNombreDeTresor() + ")";
		return aventurier == null ? toString + "\t\t" : toString + "A(" + this.getAventurier().getNom() + ")\t";
	}
		
	@Override
	public Boolean accueillir(Aventurier aventurier) {
		Boolean aventurierAbsent = super.accueillir(aventurier);
		if (aventurierAbsent) {
			if(nombreDeTresor > 0 ) {
				System.out.println("Bravo " + aventurier.getNom() + " !!! Tu as trouv� un tr�sor!!!");
				nombreDeTresor--;
				aventurier.setNombreDeTresor(aventurier.getNombreDeTresor() + 1);
			} else {
				System.out.println("Huuuum " + aventurier.getNom() + "... La terre semble avoir �t� retourn� ici.");
			}
		}
		return aventurierAbsent;		
	}
}
