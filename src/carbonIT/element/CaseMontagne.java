package carbonIT.element;

public class CaseMontagne extends Case{

	public CaseMontagne(Integer x, Integer y) {
		super(x, y);
	}

	@Override
	public String toString() {
		return "M\t\t";
	}
	
	@Override
	public Boolean accueillir(Aventurier aventurier) {
		System.out.println("Les montagnes sont infranchissables " + aventurier.getNom() + "...");
		return false;		
	}

}
